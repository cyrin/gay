'''
Intervals to do analytics on: hourly, daily, monthly
Maximum storage: ??? (hard capped by twitter's 3k limit)
To store: ['datetime'] = gay_score
Datetime: need hour / day / month / year
Datastore: pickle !
API: https://apps.twitter.com/app/6661968

-Inputs-

Tweets
Words that are gay

-Output-

Dictionary - gay / datetime
Graph - gay / datetime
'''

# builtin
import re
# external
import yaml

class analytics (object):
    '''performs gay analytics'''

    def __init__ (self):
        self.gay_words = yaml.load(file('config/gay_words.yaml', 'r'))

    def score(content_path='data/user_tweets.json'):
        '''scores gay content'''
        with open(content_path, 'r') as readfile:
            list_content = json.load(readfile)
        content = str()
        for item in list_content: content += item
        content = re.sub('?!_*^()%@#',' ',content.lower())
        gay_score = 0
        for score, word_list in self.gay_words.items():
            for word in word_list:
                if word in content: gay_score += score
        print(gay_score)
        return(gay_score)

    def rank():
        pass

if __name__ == '__main__':
    run = analytics().score()
