'''
for collecting info form twitter dot com

API: https://apps.twitter.com/app/6661968
'''

# builtin
import os
import json
# external
import tweepy
# custom
import ENV

class collector (object):
    '''for collecting info form twitter dot com'''

    def __init__ (self):
        # initialize use of twitter api
        get = os.environ.get
        if not get('KEY'): env = ENV.ENV()
        auth = tweepy.OAuthHandler(get('KEY'), get('API_SECRET'))
        auth.set_access_token(get('TOKEN'), get('TOKEN_SECRET'))
        self.api = tweepy.API(auth)

    def public_timeline (self):
        # gets user's public timeline (i.e. with tweets of others)
        public_timeline = self.api.home_timeline()
        for tweet in public_timeline:
            try: print(tweet.text)
            except UnicodeError: pass #ick
        return public_timeline

    def user_timeline (self, user=''):
        user_tweets = list()
        for api_object in self.api.user_timeline():
            user_tweets.append(api_object.text)
            print(api_object.text)
        with open('data/user_tweets.json', 'w') as outfile:
            json.dump(user_tweets, outfile, indent=4, separators=(',', ': '))
        return user_tweets

if __name__ == '__main__':
    run = collector().user_timeline()
